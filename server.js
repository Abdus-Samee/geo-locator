const path = require('path')
const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const geocoder = require('./geocoder')

dotenv.config({path: './config/config.env'})

const app = express()
const port = process.env.PORT

app.use(express.json())
app.use(cors())
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');

//database connection
try{
    const conn = async () => {
        await mongoose.connect(process.env.MONGO_URI, {useNewUrlParser : true, useUnifiedTopology: true, useFindAndModify: false });
        console.log('MongoDB connected')
    }

    conn()
}catch(e){
    console.error(e)
    process.exit(1)
}

//schema creation
const productSchema = new mongoose.Schema({
    product: {
      type: String,
      required: [true, 'Adding an available product is a must'],
      trim: true,
    },
    description: {
        type: String,
        required: [true, 'Please provide specifications of your product'],
        trim: true,
    },
    address: {
        type: String,
        required: [true, 'Please enter an address'],
    },
    location: {
        type: {
            type: String,
            enum: ['Point']
        },
        coordinates: {
            type: [Number],
            index: '2dsphere'
        },
        formattedAddress: String,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

productSchema.pre('save', async function(next){
    const loc = await geocoder.geocode(this.address)
    this.location = {
        type: 'Point',
        coordinates: [loc[0].longitude, loc[0].latitude],
        formattedAddress: loc[0].formattedAddress
    }

    const l = await geocoder.geocode('Palashi, Dhaka')

    next()
})

// const getLoc = async (address) => {
//     const loc = await geocoder.geocode(address)
//     const location = {
//         type: 'Point',
//         coordinates: [loc[0].longitude, loc[0].latitude],
//         formattedAddress: loc[0].formattedAddress
//     }

//     return location
// }

//creation of model
const Product = mongoose.model('Product', productSchema);

app.get('/', (req, res) => {
    res.render('index')
})

app.route('/add')
   .get((req, res) => {
        res.render('add')
   })
   .post((req, res) => {
       const product = req.body.product
       const description = req.body.description
       const address = req.body.road + ', ' + req.body.area + ', ' + req.body.district + ', ' + req.body.country

       const obj = new Product({
           product: product,
           description: description,
           address: address,
       })

       obj.save()

       res.redirect('/')
   })

app.get('/products', (req, res) => {
    Product.find({}, (err, products) => {
        if(!err){
            res.json(products)
        }else{
            res.render('error')
        }
    })
})

app.listen(port, () => console.log(`Server running in port ${port}...`))
